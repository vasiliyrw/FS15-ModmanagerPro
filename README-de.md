# FS15-ModmanagerPro

Nicht das erste Modverwaltungstool. Und vielleicht auch nicht das letzte. 
Aber vielleicht ein etwas schlaueres Tool.

![FS-15 ModManagerPro Screenshot](screenshots/main.png)

## Wozu noch ein Modverwaltungsprogramm?

Mit FS15-ModmanagerPro kannst du deine Mods in Profilen zusammenfassen und 
einzeln aktivieren bzw. abschalten. Es gibt andere Tools, die das auch können. 
FS15-ModManagerPro macht es nur etwas schlauer.

## Warum du den FS15-ModManagerPro benutzen solltest

Die Mod-Dateien werden mit FS15-ModmanagerPro nur ein einziges Mal auf der 
Festplatte gespeichert - unabhängig von der Anzahl der angelegten Profile. 
Andere Mod-Verwaltungs-Tools erstellen pro Mod-Profil einen eigenen Ordner und 
kopieren die benötigten Mods in diesen Ordner. FS15-ModmanagerPro nutzt jedoch 
eine Funktion aktueller Dateisysteme (ab Windows Vista), die es erlaubt, statt 
einer Kopie einer Datei einen sogenannten symblischen Link auf die 
Originaldatei anzulegen. 
Für den Landwirtschafts Simulator macht das keinen Unterschied. Für den freien 
Platz auf deiner Festplatte macht das (insbesondere bei vielen großen Mods 
und/oder vielen Profilen) durchaus einen Unterschied. 
Darüber hinaus hast du die volle Kontrolle über deine Modsammlung. Updates 
oder verschiedene Versionen eines Mods in unterschiedlichen Profilen sind
kein Problem.

  * Es gibt keine Begrenzung bei der Anzahl der Mods oder der Profile.
  * Du musst dich nur um die Pflege eines einzigen Ordners (Dein Mod-Archiv) 
  kümmern.
  * FS15-ModmanagerPro ist ultraschnell. Das Erstellen hunderter symbolischer 
  Links dauert nur einie Millisekunden.
  * Wenn du ein Update für ein Mod herunter geladen hast: Kein Problem. Ersetze 
  die Datei in deinem Mod-Archiv - und fertig. Die neue Mod ist sofort in allen
  Profilen aktiv.
  * Übersichtliche Anzeige deiner Mods und Profile.
  * Mod-Einstellungsdateien (in Form von *.xml) kannst du ebenfalls in deine 
  Profile verlinken. So hast du in allen Profilen automatisch dieselben 
  Einstellungen!
  * Detaillierte Informationen zu jedem Mod inkl. Bildanzeige.
  * FS15-ModmanagerPro ist OpenSource Software, du kannst sie frei benutzen, 
  weitergeben oder auch verändern.
  * FS15-ModmanagerPro wurde mehrsprachig entwickelt (Englisch, Spanisch and 
  Deutsch implementiert). Danke an PromGames für die Spanische Übersetzung!

![FS-15 ModManagerPro Mod Info Screenshot](screenshots/FarmingSimulator2015_modmanager-pro_modinfo.png)

## Voraussetzungen

Eine Installation des Landwirtschafts Simulators 2015 wird natürlich vorausgesetzt. 
Darüber hinaus wird das Microsoft .NET Framework 4.5.1 und eine NTFS-formatierte 
Partition vorausgesetzt.

## Installation

Der Download enthält eine `setup.exe` Datei, die sich in gewohnter Weise 
installieren lässt.

Das Programm leitet dich beim ersten Start durch die weiteren Schritte.

## Einschränkungen

FS15-ModmanagerPro funktioniert nur, wenn du dem Programm Administratorrechte 
einräumst. Das ist ein Problem, das sich leider nicht lösen lässt, weil das 
Anlegen von symbolischen Links (s.o.) nur mit Administratorrechten möglich ist.

## Bekannte Probleme

Das Installationsprogramm hat gelegentlich Probleme, eine bestehende Installation 
der vcredis2015 zu erkennen. In diesem Fall kann die Installation des vcredis 
abgebrochen werden.


## Homepage

Quellcode, Fehlermeldungen, Erweiterungswünsche auf [Gitlab](https://gitlab.com/m-busche/FS15-ModmanagerPro).