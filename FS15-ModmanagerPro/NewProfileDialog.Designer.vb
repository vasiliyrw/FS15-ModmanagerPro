﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NewProfileDialog
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NewProfileDialog))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtProfileName = New System.Windows.Forms.TextBox()
        Me.combobCopySource = New System.Windows.Forms.ComboBox()
        Me.checkbCopyOf = New System.Windows.Forms.CheckBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.checkbSGCopyOf = New System.Windows.Forms.CheckBox()
        Me.combobCopySGSource = New System.Windows.Forms.ComboBox()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'txtProfileName
        '
        resources.ApplyResources(Me.txtProfileName, "txtProfileName")
        Me.txtProfileName.Name = "txtProfileName"
        '
        'combobCopySource
        '
        Me.combobCopySource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.combobCopySource.FormattingEnabled = True
        resources.ApplyResources(Me.combobCopySource, "combobCopySource")
        Me.combobCopySource.Name = "combobCopySource"
        '
        'checkbCopyOf
        '
        resources.ApplyResources(Me.checkbCopyOf, "checkbCopyOf")
        Me.checkbCopyOf.Name = "checkbCopyOf"
        Me.checkbCopyOf.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Controls.Add(Me.btnOK)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Name = "Panel1"
        '
        'btnOK
        '
        resources.ApplyResources(Me.btnOK, "btnOK")
        Me.btnOK.Name = "btnOK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        resources.ApplyResources(Me.btnCancel, "btnCancel")
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'checkbSGCopyOf
        '
        resources.ApplyResources(Me.checkbSGCopyOf, "checkbSGCopyOf")
        Me.checkbSGCopyOf.Name = "checkbSGCopyOf"
        Me.checkbSGCopyOf.UseVisualStyleBackColor = True
        '
        'combobCopySGSource
        '
        Me.combobCopySGSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.combobCopySGSource, "combobCopySGSource")
        Me.combobCopySGSource.FormattingEnabled = True
        Me.combobCopySGSource.Name = "combobCopySGSource"
        '
        'NewProfileDialog
        '
        Me.AcceptButton = Me.btnOK
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.checkbCopyOf)
        Me.Controls.Add(Me.checkbSGCopyOf)
        Me.Controls.Add(Me.combobCopySource)
        Me.Controls.Add(Me.combobCopySGSource)
        Me.Controls.Add(Me.txtProfileName)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "NewProfileDialog"
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents txtProfileName As TextBox
    Friend WithEvents combobCopySource As ComboBox
    Friend WithEvents checkbCopyOf As CheckBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnOK As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents checkbSGCopyOf As System.Windows.Forms.CheckBox
    Friend WithEvents combobCopySGSource As System.Windows.Forms.ComboBox
End Class
