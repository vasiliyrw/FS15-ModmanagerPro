﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Settings
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Settings))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.txtConfigFile = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnOFDSettings = New System.Windows.Forms.Button()
        Me.btnFBModrepo = New System.Windows.Forms.Button()
        Me.txtModRepo = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.btnOFDExecutable = New System.Windows.Forms.Button()
        Me.txtGameExecutable = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        resources.ApplyResources(Me.TableLayoutPanel1, "TableLayoutPanel1")
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        '
        'OK_Button
        '
        resources.ApplyResources(Me.OK_Button, "OK_Button")
        Me.OK_Button.Name = "OK_Button"
        '
        'Cancel_Button
        '
        resources.ApplyResources(Me.Cancel_Button, "Cancel_Button")
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Name = "Cancel_Button"
        '
        'txtConfigFile
        '
        resources.ApplyResources(Me.txtConfigFile, "txtConfigFile")
        Me.txtConfigFile.Name = "txtConfigFile"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'btnOFDSettings
        '
        resources.ApplyResources(Me.btnOFDSettings, "btnOFDSettings")
        Me.btnOFDSettings.Name = "btnOFDSettings"
        Me.btnOFDSettings.UseVisualStyleBackColor = True
        '
        'btnFBModrepo
        '
        resources.ApplyResources(Me.btnFBModrepo, "btnFBModrepo")
        Me.btnFBModrepo.Name = "btnFBModrepo"
        Me.btnFBModrepo.UseVisualStyleBackColor = True
        '
        'txtModRepo
        '
        resources.ApplyResources(Me.txtModRepo, "txtModRepo")
        Me.txtModRepo.Name = "txtModRepo"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'FolderBrowserDialog1
        '
        resources.ApplyResources(Me.FolderBrowserDialog1, "FolderBrowserDialog1")
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        resources.ApplyResources(Me.OpenFileDialog1, "OpenFileDialog1")
        '
        'btnOFDExecutable
        '
        resources.ApplyResources(Me.btnOFDExecutable, "btnOFDExecutable")
        Me.btnOFDExecutable.Name = "btnOFDExecutable"
        Me.btnOFDExecutable.UseVisualStyleBackColor = True
        '
        'txtGameExecutable
        '
        resources.ApplyResources(Me.txtGameExecutable, "txtGameExecutable")
        Me.txtGameExecutable.Name = "txtGameExecutable"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'Settings
        '
        Me.AcceptButton = Me.OK_Button
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.Controls.Add(Me.btnOFDExecutable)
        Me.Controls.Add(Me.txtGameExecutable)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnFBModrepo)
        Me.Controls.Add(Me.txtModRepo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnOFDSettings)
        Me.Controls.Add(Me.txtConfigFile)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Settings"
        Me.ShowInTaskbar = False
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents txtConfigFile As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnOFDSettings As Button
    Friend WithEvents btnFBModrepo As Button
    Friend WithEvents txtModRepo As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents btnOFDExecutable As Button
    Friend WithEvents txtGameExecutable As TextBox
    Friend WithEvents Label3 As Label
End Class
