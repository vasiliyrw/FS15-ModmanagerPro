﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnActivateProfile = New System.Windows.Forms.Button()
        Me.DateiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProfileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewRpfileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RenameSelectedProfileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteSelectedProfileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenSelectedprofileFolderInExplorerToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EinstellungenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenmodRepositoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshmodsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResetGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BeendenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FindModToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FindNextToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ActivateAllModsForThisProfileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeactivateAllModsForThisProfileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowModinformationCrtlIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteSelectedModToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActivethisModInAllProfilesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WelcomeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.NewProfileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RenameProfileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteThisProfileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenSelectedProfileFolderInExplorerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ActivateAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeactivateAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ModInfoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteSelectedModToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActivethisModInAllProfilesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnStartFS15 = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.TSSLProfile = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSSLActiveMods = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSSLTotalMods = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSave
        '
        resources.ApplyResources(Me.btnSave, "btnSave")
        Me.btnSave.Name = "btnSave"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnActivateProfile
        '
        resources.ApplyResources(Me.btnActivateProfile, "btnActivateProfile")
        Me.btnActivateProfile.Name = "btnActivateProfile"
        Me.btnActivateProfile.UseVisualStyleBackColor = True
        '
        'DateiToolStripMenuItem
        '
        Me.DateiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProfileToolStripMenuItem, Me.EinstellungenToolStripMenuItem, Me.OpenmodRepositoryToolStripMenuItem, Me.RefreshmodsToolStripMenuItem, Me.ResetGameToolStripMenuItem, Me.BeendenToolStripMenuItem})
        Me.DateiToolStripMenuItem.Name = "DateiToolStripMenuItem"
        resources.ApplyResources(Me.DateiToolStripMenuItem, "DateiToolStripMenuItem")
        '
        'ProfileToolStripMenuItem
        '
        Me.ProfileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewRpfileToolStripMenuItem, Me.RenameSelectedProfileToolStripMenuItem, Me.DeleteSelectedProfileToolStripMenuItem, Me.OpenSelectedprofileFolderInExplorerToolStripMenuItem1})
        Me.ProfileToolStripMenuItem.Name = "ProfileToolStripMenuItem"
        resources.ApplyResources(Me.ProfileToolStripMenuItem, "ProfileToolStripMenuItem")
        '
        'NewRpfileToolStripMenuItem
        '
        Me.NewRpfileToolStripMenuItem.Name = "NewRpfileToolStripMenuItem"
        resources.ApplyResources(Me.NewRpfileToolStripMenuItem, "NewRpfileToolStripMenuItem")
        '
        'RenameSelectedProfileToolStripMenuItem
        '
        Me.RenameSelectedProfileToolStripMenuItem.Name = "RenameSelectedProfileToolStripMenuItem"
        resources.ApplyResources(Me.RenameSelectedProfileToolStripMenuItem, "RenameSelectedProfileToolStripMenuItem")
        '
        'DeleteSelectedProfileToolStripMenuItem
        '
        Me.DeleteSelectedProfileToolStripMenuItem.Name = "DeleteSelectedProfileToolStripMenuItem"
        resources.ApplyResources(Me.DeleteSelectedProfileToolStripMenuItem, "DeleteSelectedProfileToolStripMenuItem")
        '
        'OpenSelectedprofileFolderInExplorerToolStripMenuItem1
        '
        Me.OpenSelectedprofileFolderInExplorerToolStripMenuItem1.Name = "OpenSelectedprofileFolderInExplorerToolStripMenuItem1"
        resources.ApplyResources(Me.OpenSelectedprofileFolderInExplorerToolStripMenuItem1, "OpenSelectedprofileFolderInExplorerToolStripMenuItem1")
        '
        'EinstellungenToolStripMenuItem
        '
        Me.EinstellungenToolStripMenuItem.Name = "EinstellungenToolStripMenuItem"
        resources.ApplyResources(Me.EinstellungenToolStripMenuItem, "EinstellungenToolStripMenuItem")
        '
        'OpenmodRepositoryToolStripMenuItem
        '
        Me.OpenmodRepositoryToolStripMenuItem.Name = "OpenmodRepositoryToolStripMenuItem"
        resources.ApplyResources(Me.OpenmodRepositoryToolStripMenuItem, "OpenmodRepositoryToolStripMenuItem")
        '
        'RefreshmodsToolStripMenuItem
        '
        Me.RefreshmodsToolStripMenuItem.Name = "RefreshmodsToolStripMenuItem"
        resources.ApplyResources(Me.RefreshmodsToolStripMenuItem, "RefreshmodsToolStripMenuItem")
        '
        'ResetGameToolStripMenuItem
        '
        Me.ResetGameToolStripMenuItem.Name = "ResetGameToolStripMenuItem"
        resources.ApplyResources(Me.ResetGameToolStripMenuItem, "ResetGameToolStripMenuItem")
        '
        'BeendenToolStripMenuItem
        '
        Me.BeendenToolStripMenuItem.Name = "BeendenToolStripMenuItem"
        resources.ApplyResources(Me.BeendenToolStripMenuItem, "BeendenToolStripMenuItem")
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DateiToolStripMenuItem, Me.EditToolStripMenuItem, Me.HelpToolStripMenuItem})
        resources.ApplyResources(Me.MenuStrip1, "MenuStrip1")
        Me.MenuStrip1.Name = "MenuStrip1"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FindModToolStripMenuItem, Me.FindNextToolStripMenuItem, Me.ToolStripSeparator3, Me.ActivateAllModsForThisProfileToolStripMenuItem, Me.DeactivateAllModsForThisProfileToolStripMenuItem, Me.ShowModinformationCrtlIToolStripMenuItem, Me.DeleteSelectedModToolStripMenuItem, Me.ActivethisModInAllProfilesToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        resources.ApplyResources(Me.EditToolStripMenuItem, "EditToolStripMenuItem")
        '
        'FindModToolStripMenuItem
        '
        Me.FindModToolStripMenuItem.Name = "FindModToolStripMenuItem"
        resources.ApplyResources(Me.FindModToolStripMenuItem, "FindModToolStripMenuItem")
        '
        'FindNextToolStripMenuItem
        '
        Me.FindNextToolStripMenuItem.Name = "FindNextToolStripMenuItem"
        resources.ApplyResources(Me.FindNextToolStripMenuItem, "FindNextToolStripMenuItem")
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        resources.ApplyResources(Me.ToolStripSeparator3, "ToolStripSeparator3")
        '
        'ActivateAllModsForThisProfileToolStripMenuItem
        '
        Me.ActivateAllModsForThisProfileToolStripMenuItem.Name = "ActivateAllModsForThisProfileToolStripMenuItem"
        resources.ApplyResources(Me.ActivateAllModsForThisProfileToolStripMenuItem, "ActivateAllModsForThisProfileToolStripMenuItem")
        '
        'DeactivateAllModsForThisProfileToolStripMenuItem
        '
        Me.DeactivateAllModsForThisProfileToolStripMenuItem.Name = "DeactivateAllModsForThisProfileToolStripMenuItem"
        resources.ApplyResources(Me.DeactivateAllModsForThisProfileToolStripMenuItem, "DeactivateAllModsForThisProfileToolStripMenuItem")
        '
        'ShowModinformationCrtlIToolStripMenuItem
        '
        Me.ShowModinformationCrtlIToolStripMenuItem.Name = "ShowModinformationCrtlIToolStripMenuItem"
        resources.ApplyResources(Me.ShowModinformationCrtlIToolStripMenuItem, "ShowModinformationCrtlIToolStripMenuItem")
        '
        'DeleteSelectedModToolStripMenuItem
        '
        Me.DeleteSelectedModToolStripMenuItem.Name = "DeleteSelectedModToolStripMenuItem"
        resources.ApplyResources(Me.DeleteSelectedModToolStripMenuItem, "DeleteSelectedModToolStripMenuItem")
        '
        'ActivethisModInAllProfilesToolStripMenuItem
        '
        Me.ActivethisModInAllProfilesToolStripMenuItem.Name = "ActivethisModInAllProfilesToolStripMenuItem"
        resources.ApplyResources(Me.ActivethisModInAllProfilesToolStripMenuItem, "ActivethisModInAllProfilesToolStripMenuItem")
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.WelcomeToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        resources.ApplyResources(Me.HelpToolStripMenuItem, "HelpToolStripMenuItem")
        '
        'WelcomeToolStripMenuItem
        '
        Me.WelcomeToolStripMenuItem.Name = "WelcomeToolStripMenuItem"
        resources.ApplyResources(Me.WelcomeToolStripMenuItem, "WelcomeToolStripMenuItem")
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        resources.ApplyResources(Me.AboutToolStripMenuItem, "AboutToolStripMenuItem")
        '
        'Panel1
        '
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Panel1.Name = "Panel1"
        Me.ToolTip1.SetToolTip(Me.Panel1, resources.GetString("Panel1.ToolTip"))
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewProfileToolStripMenuItem, Me.RenameProfileToolStripMenuItem, Me.DeleteThisProfileToolStripMenuItem, Me.OpenSelectedProfileFolderInExplorerToolStripMenuItem, Me.ToolStripSeparator1, Me.ActivateAllToolStripMenuItem, Me.DeactivateAllToolStripMenuItem, Me.ToolStripSeparator2, Me.ModInfoToolStripMenuItem, Me.DeleteSelectedModToolStripMenuItem1, Me.ActivethisModInAllProfilesToolStripMenuItem1})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        resources.ApplyResources(Me.ContextMenuStrip1, "ContextMenuStrip1")
        '
        'NewProfileToolStripMenuItem
        '
        Me.NewProfileToolStripMenuItem.Name = "NewProfileToolStripMenuItem"
        resources.ApplyResources(Me.NewProfileToolStripMenuItem, "NewProfileToolStripMenuItem")
        '
        'RenameProfileToolStripMenuItem
        '
        Me.RenameProfileToolStripMenuItem.Name = "RenameProfileToolStripMenuItem"
        resources.ApplyResources(Me.RenameProfileToolStripMenuItem, "RenameProfileToolStripMenuItem")
        '
        'DeleteThisProfileToolStripMenuItem
        '
        Me.DeleteThisProfileToolStripMenuItem.Name = "DeleteThisProfileToolStripMenuItem"
        resources.ApplyResources(Me.DeleteThisProfileToolStripMenuItem, "DeleteThisProfileToolStripMenuItem")
        '
        'OpenSelectedProfileFolderInExplorerToolStripMenuItem
        '
        Me.OpenSelectedProfileFolderInExplorerToolStripMenuItem.Name = "OpenSelectedProfileFolderInExplorerToolStripMenuItem"
        resources.ApplyResources(Me.OpenSelectedProfileFolderInExplorerToolStripMenuItem, "OpenSelectedProfileFolderInExplorerToolStripMenuItem")
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        resources.ApplyResources(Me.ToolStripSeparator1, "ToolStripSeparator1")
        '
        'ActivateAllToolStripMenuItem
        '
        Me.ActivateAllToolStripMenuItem.Name = "ActivateAllToolStripMenuItem"
        resources.ApplyResources(Me.ActivateAllToolStripMenuItem, "ActivateAllToolStripMenuItem")
        '
        'DeactivateAllToolStripMenuItem
        '
        Me.DeactivateAllToolStripMenuItem.Name = "DeactivateAllToolStripMenuItem"
        resources.ApplyResources(Me.DeactivateAllToolStripMenuItem, "DeactivateAllToolStripMenuItem")
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        resources.ApplyResources(Me.ToolStripSeparator2, "ToolStripSeparator2")
        '
        'ModInfoToolStripMenuItem
        '
        Me.ModInfoToolStripMenuItem.Name = "ModInfoToolStripMenuItem"
        resources.ApplyResources(Me.ModInfoToolStripMenuItem, "ModInfoToolStripMenuItem")
        '
        'DeleteSelectedModToolStripMenuItem1
        '
        Me.DeleteSelectedModToolStripMenuItem1.Name = "DeleteSelectedModToolStripMenuItem1"
        resources.ApplyResources(Me.DeleteSelectedModToolStripMenuItem1, "DeleteSelectedModToolStripMenuItem1")
        '
        'ActivethisModInAllProfilesToolStripMenuItem1
        '
        Me.ActivethisModInAllProfilesToolStripMenuItem1.Name = "ActivethisModInAllProfilesToolStripMenuItem1"
        resources.ApplyResources(Me.ActivethisModInAllProfilesToolStripMenuItem1, "ActivethisModInAllProfilesToolStripMenuItem1")
        '
        'btnStartFS15
        '
        resources.ApplyResources(Me.btnStartFS15, "btnStartFS15")
        Me.btnStartFS15.Name = "btnStartFS15"
        Me.btnStartFS15.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSSLProfile, Me.TSSLActiveMods, Me.TSSLTotalMods})
        resources.ApplyResources(Me.StatusStrip1, "StatusStrip1")
        Me.StatusStrip1.Name = "StatusStrip1"
        '
        'TSSLProfile
        '
        Me.TSSLProfile.Name = "TSSLProfile"
        resources.ApplyResources(Me.TSSLProfile, "TSSLProfile")
        '
        'TSSLActiveMods
        '
        Me.TSSLActiveMods.Name = "TSSLActiveMods"
        resources.ApplyResources(Me.TSSLActiveMods, "TSSLActiveMods")
        '
        'TSSLTotalMods
        '
        Me.TSSLTotalMods.Name = "TSSLTotalMods"
        resources.ApplyResources(Me.TSSLTotalMods, "TSSLTotalMods")
        '
        'Form1
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnStartFS15)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnActivateProfile)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Form1"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSave As Button
    Friend WithEvents btnActivateProfile As Button
    Friend WithEvents DateiToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EinstellungenToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BeendenToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents Panel1 As Panel
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents WelcomeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnStartFS15 As Button
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents ActivateAllToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DeactivateAllToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ActivateAllModsForThisProfileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DeactivateAllModsForThisProfileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OpenmodRepositoryToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OpenSelectedProfileFolderInExplorerToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ResetGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewProfileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RenameProfileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProfileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NewRpfileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RenameSelectedProfileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OpenSelectedprofileFolderInExplorerToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents DeleteThisProfileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DeleteSelectedProfileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RefreshmodsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModInfoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents TSSLProfile As ToolStripStatusLabel
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ShowModinformationCrtlIToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DeleteSelectedModToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DeleteSelectedModToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ActivethisModInAllProfilesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ActivethisModInAllProfilesToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents FindModToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TSSLTotalMods As ToolStripStatusLabel
    Friend WithEvents TSSLActiveMods As ToolStripStatusLabel
    Friend WithEvents FindNextToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
End Class
