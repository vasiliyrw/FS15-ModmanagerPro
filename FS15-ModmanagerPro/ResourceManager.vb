﻿Imports System.Globalization
Imports FS15_ModmanagerPro.My.Resources

''' <summary>
''' Resource Manager Class
''' </summary>
Public Class ResourceManager
    ''' <summary>
    ''' Read the resource file and return the string of specified culture
    ''' </summary>
    ''' <param name="data">String Data to be read</param>
    ''' <param name="cultureInfo">Type of Culture</param>
    ''' <returns></returns>
    Public Shared Function GetString(data As String, cultureInfo As CultureInfo) As String
        Return CommonResource.ResourceManager.GetString(data, cultureInfo)
    End Function
End Class