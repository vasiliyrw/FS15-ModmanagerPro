﻿Imports System.Windows.Forms
Imports System.Collections.Specialized
Imports System.Globalization
Imports System.Threading

Public Class welcome
    Dim ci As CultureInfo = CultureInfo.CurrentCulture
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub welcome_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' This may be the first start, let me tell you some things!
        Dim sText As String = ""
        If ci.ToString = "de-DE" Then
            sText = String.Format(
                "Du benutzt den FS15-ModmanagerPro zu ersten Mal? Dann lies dies bitte sorgfältig!{0}{0}" +
                "(1) FS15-ModmanagerPro benutzt symbolische Dateisystem-Links. Das spart einen Haufen Platz auf deiner Festplatte.{0}" +
                "Leider ist Microsoft der Ansicht, dass das gefährlich sein kann (" +
                "womit sie in einigen Fällen natürlich auch Recht haben).{0}" +
                "Deshalb musst du diesem Programm vertrauen und ihm Administratorrechte gewähren.{0}{0}" +
                "(2) Bereite dein System vor:{0}" +
                "(2a) Erstelle ein Verzeichnis an einem Ort deiner Wahl und verlege alle deine FarmingSimlator Mod Dateien von ihrem ursprünglichen Ort " +
                "(normalerweise '<Eigene Dateien>\my games\FarmingSimulator 2015\mods') in diesen neuen Ordner.{0}" +
                "(2b) Diesen Ordner nennen wir deinen Mod-Archiv Ordner. Diesen Ordner musst du in den Einstellungen von FS15-ModmanagerPro angeben.{0}{0}" +
                "Falls irgendetwas schief geht: Ich kann nichts dafür! Ich werde auch niemals deine Spielspeicher (Savegames) anfassen.{0}{0}" +
                "Wenn du dein System jetzt erstmal vorbereiten möchtest, klicke auf Abbruch um das Programm zu verlassen.{0}{0}" +
                "Wenn du dies später nochmal lesen möchtest, klicke im Hauptmenu auf 'Hilfe' - 'Moin'.", vbCrLf)
        ElseIf ci.ToString = "es-ES" Then
            sText = String.Format(
                "Esta parece ser la primera vez que utilizas FS15-ModmanagerPro, por lo que te rogamos que leas detenidamente estas instrucciones!{0}{0}" +
                "(1) FS15-ModmanagerPro utiliza el sistema de enlaces simbólicos (SymLinks). Este método te permitira ahorrar mucho espacio en tu disco duro.{0}" +
                "Desafortunadamente, Microsoft piensa que los symlinks pueden ser dañinos para tu sistema windows (" +
                "y probablemente esté en lo cierto en algunos casos).{0}" +
                "Por lo tanto, deberás confiar en este programa y darle acceso de administrador.{0}{0}" +
                "(2) Pasos para preparar el sistema:{0}" +
                "(2a) Define y crea una carpeta para el repositorio de mods y mueve allí todos los mods y mapas que utilizas " +
                "(normalmente moveras desde '<Documentos>\my games\FarmingSimulator 2015\mods') a la nueva carpeta creada.{0}" +
                "(2b) Esta nueva carpeta es tu almacen de mods y mapas. Dejame saber cual es seleccionandolo en el siguiente diálogo.{0}{0}" +
                "Si cualquier cosa ocurre, no me eches a mi la culpa, yo no voy a tocar tus savegames.{0}{0}" +
                "Si necesitas crear la nueva carpeta ahora, click 'Cancel' y saldrás del programa.{0}{0}" +
                "Si quieres leer estas instrucciones posteriormente, podrás encontrarlas en el menú principal ('Ayuda' - 'Bienvenida').", vbCrLf)
        Else
            sText = String.Format(
                "This seems to be the first time you use FS15-ModmanagerPro, so please read this carefully!{0}{0}" +
                "(1) FS15-ModmanagerPro uses symbolic filesystem links. This method saves a lot of space on your harddisk.{0}" +
                "Unfortunately, Microsoft thinks, symbolic links can be harmful to the security of your Windows system (" +
                "and they are probably right in some cases)." +
                "Therefore, you have to trust this program and grant it Administrator rights.{0}{0}" +
                "(2) Prepare your system.{0}" +
                "(2a) Create a directory of your choice and move all your FarmingSimlator mod files from their original place " +
                "(usually '<your documents>\my games\FarmingSimulator 2015\mods') to this new folder.{0}" +
                "(2b) This new folder is called your mod repository. Let me know where it is by selecting it in the settings dialog.{0}{0}" +
                "If anything goes wrong: Don´t blame me! I will never touch your savegame files.{0}{0}" +
                "If you want to prepare your system now, click 'Cancel' to exit this program.{0}{0}" +
                "If you want to read this again later you can find it in the main menu ('Help' - 'Welcome').", vbCrLf)
        End If

        TextBox1.Text = sText
    End Sub
End Class