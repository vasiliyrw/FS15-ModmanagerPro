﻿Imports System.Xml
Imports System.Collections.Specialized
Imports System.IO

Module XMLSettings
    Public Function savesettings(ByVal sSettingsXmlFile As String, ByVal sModRepository As String, ByVal sGameExeFile As String) As Boolean
        Dim bResult As Boolean = True
        If Not Directory.Exists(Path.GetDirectoryName(Form1.sSettingsFile)) Then
            Try
                Directory.CreateDirectory(Path.GetDirectoryName(Form1.sSettingsFile))
            Catch ex As Exception
                bResult = False
            End Try
        End If
        If bResult = True Then
            Try
                Dim XMLWritersettings As XmlWriterSettings = New XmlWriterSettings()
                XMLWritersettings.Indent = True
                Using writer As XmlWriter = XmlWriter.Create(Form1.sSettingsFile, XMLWritersettings)
                    writer.WriteStartDocument()
                    writer.WriteStartElement("Settings")
                    writer.WriteStartElement("Setting")
                    writer.WriteAttributeString("gamexmlfile", sSettingsXmlFile)
                    writer.WriteEndElement()
                    writer.WriteStartElement("Setting")
                    writer.WriteAttributeString("modrepository", sModRepository)
                    writer.WriteEndElement()
                    writer.WriteStartElement("Setting")
                    writer.WriteAttributeString("gameexefile", sGameExeFile)
                    writer.WriteEndElement()
                    writer.WriteEndElement()
                    writer.WriteEndDocument()
                End Using
            Catch ex As Exception
                bResult = False
            End Try
        End If
        Return bResult
    End Function

    Public Function readsettings() As String()
        Dim sValues(2) As String
        If Not File.Exists(Form1.sSettingsFile) Then
            MsgBox("Settings file not found!", MsgBoxStyle.Critical)
            sValues(0) = ""
            Return sValues
            Exit Function
        End If

        Dim XMLReader As Xml.XmlReader = New Xml.XmlTextReader(Form1.sSettingsFile)
        With XMLReader
            Do While .Read
                Select Case .NodeType
                    Case Xml.XmlNodeType.Element
                        If .AttributeCount > 0 Then
                            While .MoveToNextAttribute
                                If .Name = "gamexmlfile" Then
                                    sValues(0) = .Value
                                End If
                                If .Name = "modrepository" Then
                                    sValues(1) = .Value
                                End If
                                If .Name = "gameexefile" Then
                                    sValues(2) = .Value
                                End If
                            End While
                        End If
                End Select
            Loop
        End With
        XMLReader.Close()
        Return sValues
    End Function

    Public Function readprofiles(ByVal sProfileFolder As String) As String()
        Dim sProfiles() As String = Directory.GetDirectories(sProfileFolder)
        If sProfiles IsNot Nothing Then
            Try
                For iCount As Integer = 0 To sProfiles.Length - 1
                    Dim split As String() = sProfiles(iCount).Split("\")
                    sProfiles(iCount) = split(split.Length - 1)
                Next

            Catch ex As Exception

            End Try
        End If
        Return sProfiles
    End Function
End Module
