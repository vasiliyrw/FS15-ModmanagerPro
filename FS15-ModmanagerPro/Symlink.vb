﻿Imports System.ComponentModel
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Security.AccessControl
Imports System.Text
Imports Microsoft.Win32.SafeHandles
''' <summary>
''' Stellt Funktionen zum erstellen, löschen und auslesen von symbolischen Links bereit.
''' </summary>
Public NotInheritable Class SymbolicLink
    Private Sub New()
    End Sub
#Region "WinAPI"

    <DllImport("kernel32.dll", EntryPoint:="CreateSymbolicLinkW", CharSet:=CharSet.Unicode, SetLastError:=True)>
    Private Shared Function CreateSymbolicLink(<[In]> lpSymlinkFileName As String, <[In]> lpTargetFileName As String, <[In]> dwFlags As Integer) As Boolean
    End Function

    <DllImport("kernel32.dll", EntryPoint:="GetFinalPathNameByHandleW", CharSet:=CharSet.Unicode, SetLastError:=True)>
    Private Shared Function GetFinalPathNameByHandle(<[In]> hFile As IntPtr, <Out> lpszFilePath As StringBuilder, <[In]> cchFilePath As Integer, <[In]> dwFlags As Integer) As Integer
    End Function

    <DllImport("kernel32.dll", EntryPoint:="CreateFileW", CharSet:=CharSet.Unicode, SetLastError:=True)>
    Private Shared Function CreateFile(lpFileName As String, dwDesiredAccess As Integer, dwShareMode As Integer, SecurityAttributes As IntPtr, dwCreationDisposition As Integer, dwFlagsAndAttributes As Integer,
        hTemplateFile As IntPtr) As SafeFileHandle
    End Function

    Private Const CREATION_DISPOSITION_OPEN_EXISTING As Integer = 3
    Private Const FILE_FLAG_BACKUP_SEMANTICS As Integer = &H2000000
    Private Const SYMBOLIC_LINK_FLAG_DIRECTORY As Integer = &H1

#End Region

    ''' <summary>
    ''' Erstellt einen symbolischen Link.
    ''' </summary>
    ''' <param name="target">Das Ziel auf das der Link verweisen soll.</param>
    ''' <param name="path">Der Pfad des symbolischen Links.</param>
    ''' <param name="replaceExisting"><c>True</c>, wenn eine bereits existierende Datei bzw. ein bereits existierender symbolischer Link überschrieben werden soll.</param>
    Public Shared Sub CreateSymbolicLink(target As String, path As String, replaceExisting As Boolean)
        If replaceExisting Then
            DeleteSymbolicLink(path)
        End If
        Dim result As Boolean = False
        If Directory.Exists(target) Then
            result = CreateSymbolicLink(path, target, SYMBOLIC_LINK_FLAG_DIRECTORY)
        ElseIf File.Exists(target) Then
            result = CreateSymbolicLink(path, target, 0)
        Else
            Throw New IOException("path not found")
        End If
        If Not result Then
            Throw New Win32Exception(Marshal.GetLastWin32Error())
        End If
    End Sub

    ''' <summary>
    ''' Löscht einen symbolischen Link.
    ''' </summary>
    ''' <param name="path">Der Pfad zum symbolischen Link.</param>
    Public Shared Sub DeleteSymbolicLink(path As String)
        If Directory.Exists(path) Then
            Directory.Delete(path)
        End If
        If File.Exists(path) Then
            File.Delete(path)
        End If
    End Sub

    ''' <summary>
    ''' Gibt den Pfad eines Ordners oder einer Datei zurück wobei symbolische Links und bereit gestellte NTFS Ordner aufgelöst wurden.
    ''' </summary>
    ''' <param name="path">Der Pfad zu einem Ordner oder einer Datei.</param>
    ''' <returns>Der wahre Pfad von <paramref name="path"/>.</returns>
    ''' <remarks>Sollte kein Laufwerkpfad für den NTFS-Ordner verfügbar sein, so wird der Bereitstellungspfad zurück gegeben.</remarks>
    Public Shared Function GetRealPath(path As String) As String
        If Not Directory.Exists(path) AndAlso Not File.Exists(path) Then
            Throw New IOException("Path not found")
        End If

        Dim symlink As New DirectoryInfo(path)
        'Es ist egel ob es eine Datei oder ein Ordner ist
        Dim directoryHandle As SafeFileHandle = CreateFile(symlink.FullName, 0, 2, System.IntPtr.Zero, CREATION_DISPOSITION_OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS,
            System.IntPtr.Zero)
        'Handle zur Datei/Ordner
        If directoryHandle.IsInvalid Then
            Throw New Win32Exception(Marshal.GetLastWin32Error())
        End If

        Dim result As New StringBuilder(512)
        Dim mResult As Integer = GetFinalPathNameByHandle(directoryHandle.DangerousGetHandle(), result, result.Capacity, 0)
        If mResult < 0 Then
            Throw New Win32Exception(Marshal.GetLastWin32Error())
        End If
        If result.Length >= 4 AndAlso result(0) = "\"c AndAlso result(1) = "\"c AndAlso result(2) = "?"c AndAlso result(3) = "\"c Then
            Return result.ToString().Substring(4)
        Else
            ' "\\?\" entfernen
            Return result.ToString()
        End If
    End Function

    ''' <summary>
    ''' Checks for filesystem permissions
    ''' </summary>
    ''' <param name="path"></param>
    ''' <returns></returns>
    Public Shared Function HasWritePermissionOnDir(path As String) As Boolean
        Dim writeAllow = False
        Dim writeDeny = False
        Dim accessControlList = Directory.GetAccessControl(path)
        If accessControlList Is Nothing Then
            Return False
        End If
        Dim accessRules = accessControlList.GetAccessRules(True, True, GetType(System.Security.Principal.SecurityIdentifier))
        If accessRules Is Nothing Then
            Return False
        End If

        For Each rule As FileSystemAccessRule In accessRules
            If (FileSystemRights.Write And rule.FileSystemRights) <> FileSystemRights.Write Then
                Continue For
            End If

            If rule.AccessControlType = AccessControlType.Allow Then
                writeAllow = True
            ElseIf rule.AccessControlType = AccessControlType.Deny Then
                writeDeny = True
            End If
        Next

        Return writeAllow AndAlso Not writeDeny
    End Function

    ''' <summary>
    ''' Checks if drive is formatted with NTFS filesystem.
    ''' </summary>
    ''' <param name="cDriveletter"></param>
    ''' <returns>True if NTFS</returns>
    Public Shared Function checkNTFS(ByVal cDriveletter As Char) As Boolean
        Dim d As DriveInfo = New DriveInfo(cDriveletter)
        If d.DriveFormat.ToLower.Contains("ntfs") Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
