#define VCmsg "Installing Microsoft Visual C++ Redistributable...."
#define MyAppName "FS15-ModmanagerPro"
#define MyAppSetupName "FS15-ModmanagerPro"
#define MyAppVersion "1.0.0.8"
#define MyAppPublisher "Markus Busche"
#define MyAppURL "https://gitlab.com/m-busche/FS15-ModmanagerPro/"
#define MyAppExeName "FS15-ModmanagerPro.exe"

[Setup]
AppId={{6AD728D0-72C1-4068-AEC6-635AD199F8F7}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppSetupName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
AppCopyright=Copyright � 2015 elpatron
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
AllowNoIcons=yes
LicenseFile=.\..\LICENSE
Compression=lzma
SolidCompression=yes
VersionInfoVersion={#MyAppVersion}
VersionInfoCompany=elpatron
OutputBaseFilename={#MyAppSetupName}-{#MyAppVersion}
UninstallDisplayIcon={app}\FS15-ModmanagerPro.exe
OutputDir=Output
SourceDir=.

;MinVersion default value: "0,5.0 (Windows 2000+) if Unicode Inno Setup, else 4.0,4.0 (Windows 95+)"
MinVersion=0,5.0
PrivilegesRequired=admin
ArchitecturesAllowed=x86 x64 ia64
ArchitecturesInstallIn64BitMode=x64 ia64

;Downloading and installing dependencies will only work if the memo/ready page is enabled (default behaviour)
DisableReadyPage=no
DisableReadyMemo=no

[Languages]
Name: "en"; MessagesFile: "compiler:Default.isl"
Name: "de"; MessagesFile: "compiler:Languages\German.isl"
Name: "spanish"; MessagesFile: "compiler:Languages\Spanish.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"

[Files]
; 64 Bit install
Source: "..\FS15-ModmanagerPro\bin\x64\Release\FS15-ModmanagerPro.exe"; DestDir: "{app}"; DestName: "FS15-ModmanagerPro.exe"; Flags: ignoreversion; Check: Is64BitInstallMode
Source: "..\FS15-ModmanagerPro\bin\x64\Release\de\*"; DestDir: "{app}\de\"; Flags: ignoreversion; Check: Is64BitInstallMode
Source: "..\FS15-ModmanagerPro\bin\x64\Release\es\*"; DestDir: "{app}\es\"; Flags: ignoreversion; Check: Is64BitInstallMode
Source: "..\FS15-ModmanagerPro\bin\x64\Release\*.dll"; DestDir: "{app}"; Flags: ignoreversion; Check: Is64BitInstallMode
Source: "bin\vc_redist.x64.exe"; DestDir: "{tmp}"; Flags: ignoreversion; Check: Is64BitInstallMode
; 32 Bit install
Source: "..\FS15-ModmanagerPro\bin\x86\Release\FS15-ModmanagerPro.exe"; DestDir: "{app}"; DestName: "FS15-ModmanagerPro.exe"; Flags: ignoreversion; Check: not Is64BitInstallMode
Source: "..\FS15-ModmanagerPro\bin\x86\Release\de\*"; DestDir: "{app}\de\"; Flags: ignoreversion; Check: not Is64BitInstallMode
Source: "..\FS15-ModmanagerPro\bin\x86\Release\es\*"; DestDir: "{app}\es\"; Flags: ignoreversion; Check: not Is64BitInstallMode
Source: "..\FS15-ModmanagerPro\bin\x86\Release\*.dll"; DestDir: "{app}"; Flags: ignoreversion; Check: not Is64BitInstallMode
Source: "bin\vc_redist.x86.exe"; DestDir: "{tmp}"; Flags: ignoreversion; Check: not Is64BitInstallMode
; common files
Source: "..\CHANGELOG*.*"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\README*.*"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\LICENSE*.*"; DestDir: "{app}"; Flags: ignoreversion

[Icons]
Name: "{group}\{#MyAppSetupName}"; Filename: "{app}\FS15-ModmanagerPro.exe"
Name: "{group}\{cm:UninstallProgram,{#MyAppSetupName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppSetupName}"; Filename: "{app}\FS15-ModmanagerPro.exe"; Tasks: desktopicon

[Run]
Filename: "{tmp}\vc_redist.x86.exe"; StatusMsg: "{#VCmsg}"; Check: not IsWin64 and not VCinstalled
Filename: "{tmp}\vc_redist.x64.exe"; StatusMsg: "{#VCmsg}"; Check: IsWin64 and not VCinstalled
Filename: "{app}\FS15-ModmanagerPro.exe"; Description: "{cm:LaunchProgram,{#MyAppSetupName}}"; Flags: nowait postinstall skipifsilent

[CustomMessages]
win_sp_title=Windows %1 Service Pack %2

[UninstallDelete]
Type: filesandordirs; Name: "{app}"
Type: filesandordirs; Name: "{commonappdata}\FS15-ModmanagerPro"

[Code]
function VCinstalled: Boolean;
 // By Michael Weiner <mailto:spam@cogit.net>
 // Function for Inno Setup Compiler
 // 13 November 2015
 // Returns True if Microsoft Visual C++ Redistributable is installed, otherwise False.
 // The programmer may set the year of redistributable to find; see below.
 var
  names: TArrayOfString;
  i: Integer;
  dName, key, year: String;
 begin
  // Year of redistributable to find; leave null to find installation for any year.
  year := '2015';
  Result := False;
  key := 'Software\Microsoft\Windows\CurrentVersion\Uninstall';
  // Get an array of all of the uninstall subkey names.
  if RegGetSubkeyNames(HKEY_LOCAL_MACHINE, key, names) then
   // Uninstall subkey names were found.
   begin
    i := 0
    while ((i < GetArrayLength(names)) and (Result = False)) do
     // The loop will end as soon as one instance of a Visual C++ redistributable is found.
     begin
      // For each uninstall subkey, look for a DisplayName value.
      // If not found, then the subkey name will be used instead.
      if not RegQueryStringValue(HKEY_LOCAL_MACHINE, key + '\' + names[i], 'DisplayName', dName) then
       dName := names[i];
      // See if the value contains both of the strings below.
      Result := (Pos(Trim('Visual C++ ' + year),dName) * Pos('Redistributable',dName) <> 0)
      i := i + 1;
     end;
   end;
 end;
